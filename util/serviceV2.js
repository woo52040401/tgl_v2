const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');
const Config = require('../config/ibmConfig');

exports.createService = () => {
    return new AssistantV2({
        version: Config.version,
        authenticator: new IamAuthenticator({
          apikey: Config.apikey,
        }),
        url: Config.url,
      });
};