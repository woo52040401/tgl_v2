const Config = require('../config/ibmConfig');
const ServiceV2 = require('../util/serviceV2');

exports.getBot = (req,res,next) => {
    const service = ServiceV2.createService();
    //取得session id 
    let sessionId;
    service.createSession({
        assistantId : Config.assistantId
    })
    .then( result => {
        sessionId = result.result.session_id;
        res.json({
            sessionId : sessionId,
            type : "text",
            text : "Success !"
        })
        console.log(JSON.stringify(result, null, 2));
    })
    .catch( err => {
        res.json({error : err});
    })
};

exports.postMessage = (req,res,next) => {
    const sessionId = req.body.sessionId;
    const message = req.body.message;
    const service = ServiceV2.createService();

    service.message({
        assistantId : Config.assistantId,
        sessionId : sessionId,
        input: {
            "message_type": "text",
            "text" : message
        }
    })
    .then( result => {
        res.json(JSON.stringify(result, null, 2));
        console.log(JSON.stringify(result.result.output.generic,null,2));
    })
    .catch( err => {
        res.json({error : err});
    })
};

exports.postDeleteSession = (req, res, next) => {
    const sessionId = req.body.sessionId;
    const service = ServiceV2.createService();

    service.deleteSession({
        assistantId: Config.assistantId,
        sessionId : sessionId
    })
    .then( result => {
        res.json(JSON.stringify(result, null ,2));
        console.log('Success delete');
    })
    .catch( err => {
        res.json({error : err});
    })
}

