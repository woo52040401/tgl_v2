const Log = require('../models/sqlizeConfig');

//Get方法
exports.getLogs = (req , res, next) => {
    Log.findAll()
    .then( logs => {
        res.json(logs);
    })
    .catch(err => {
        res.json({error : err});
    })
}
//Add方法
exports.postAddLog = (req, res, next) =>{
    const content = req.body.content;
    const description = req.body.description;
    const newlog = {
        content : content,
        description : description
    };
    Log.create(newlog)
    .then( () => {
        res.json(newlog);
    })
    .catch(err => {
        res.json({error : err})
    })
}
//Update方法
exports.postUpdateLog = (req ,res, next) => {
    const logId = req.params.id;
    const updatedContent = req.body.content;
    const updatedDescription = req.body.description;
    let updatedlog;

    if(!logId){
        res.json({error : 'Log id is missing'});
    }
    Log.findByPk(logId)
    .then( log => {
        log.content = updatedContent;
        log.description = updatedDescription;
        updatedlog = log;
        log.save();
    })
    .then( () => {
        res.json(updatedlog);
    })
    .catch( err => {
        res.json({error : err});
    })
}
//Delete 方法
exports.getDeleteLog = (req, res, next) => {
    const logId = req.params.id;
    console.log("Log id = " + logId);
    let deleteLog;

    if(!logId){
        res.json({error: 'logId is missing.'});
    }
    Log.findByPk(logId)
    .then(log => {
        deleteLog = log;
        return log.destroy();
    })
    .then( () => {
        res.json(deleteLog);
    })
    .catch(err => {
        res.json({error: err});
    })
}