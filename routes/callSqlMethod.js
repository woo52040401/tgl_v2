const express = require('express');
const logController = require('../controllers/crudMethod');

const router = express.Router();

//Get
router.get('/logs', logController.getLogs);
//Add
router.post('/add-log', logController.postAddLog);
//Update
router.post('/update-log/:id', logController.postUpdateLog);
//Delete
router.get('/delete-log/:id', logController.getDeleteLog);

module.exports = router;