const express = require('express');
const botController = require('../controllers/bot');


const router = express.Router();

router.get('/bot', botController.getBot);

router.post('/message', botController.postMessage);

router.post('/end-session', botController.postDeleteSession);

module.exports = router;
