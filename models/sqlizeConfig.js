const Sequelize = require('sequelize');
const sequelize = require('../util/database');

const Log = sequelize.define('log', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    content: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: Sequelize.STRING
});

module.exports = Log;