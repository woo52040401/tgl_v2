const express = require('express');
const bodyParser = require('body-parser');

const sequelize = require('./util/database');
const Config = require('./config/dbConfig');

// Include models here
const Log = require('./models/sqlizeConfig');

const app = express();
app.use(bodyParser.urlencoded({extended: false}));


// Setup template engine
app.set('view engine', 'ejs');
app.set('views', 'views');

// Routes
const logRoutes = require('./routes/callSqlMethod');
const botRoutes = require('./routes/bot');
app.use('/admin', logRoutes);
app.use('/bot', botRoutes);

// Connecting MSSQL.

sequelize
    .sync()
    .then( () => {
        return Log.findByPk(1);
    })
    .then(log => {
        if(!log){
            return Log.create({
                content: "This is  the content of test 1",
                description: 'This is the description of test 1.'
            });
        }
        return Promise.resolve(log);
    })
    .then(() => {
        console.log('Connect to SQL DB');
        app.listen(Config.webPort);
    })
    .catch(err=>{
        console.log(err);
    })
